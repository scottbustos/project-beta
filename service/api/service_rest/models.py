from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, unique=True)
class Technician(models.Model):
    name=models.CharField(max_length=200)
    employee_number=models.IntegerField(unique=True)

class Appointment(models.Model):
    customer = models.CharField(max_length=200)
    date = models.CharField(max_length=200)
    time = models.CharField(max_length=200)
    reason = models.CharField(max_length=500)
    vip = models.BooleanField(default=False, null=True, blank=True)
    vin = models.CharField(max_length=17)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT
    )
