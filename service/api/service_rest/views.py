from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .model_encoders import AppointmentEncoder, TechnicianEncoder
from .models import Appointment, Technician, AutomobileVO
# These are my importants from django and models.

# This line decorates my list appointments function to only
# retrieve and create appointments.
@require_http_methods(['GET', 'POST'])
def list_appointments(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
# If the method is GET return a list of all the appointments from the database.

    else:
        content = json.loads(request.body)
# This executes when the HTTP method is POST. It parses the JSON data sent in
# the request body and loads it into the content variable.
        try:
            technician = content["technician"]
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status=404,
            )
# This block tries to get the technician object from the database based on the
# technician ID provided in the request. If the technician doesn't exist it
# will returns a 404.
        try:
            if AutomobileVO.objects.get(vin=content["vin"]):
# Checks to see if the AutomobileVO object exists in the database
                content["vip"] = True
# If the AutomobileVO object exist this will set the content dictionary to
# True, indicatiing the vehicle is "VIP".
        except AutomobileVO.DoesNotExist:
            content["vip"] = False
# If the AutomobileVO object doesnt exist in the database it will return
# False, indicating that it is not a vip vehicle.
        appointment = Appointment.objects.create(**content)
# This line creates a new appointment using the data provided in the request
# content dictionary.
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
# This block of code returns a JsonResponse with the newly created appointment.


@require_http_methods(["GET", "PUT", "DELETE"])
def show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse({"appointment": appointment}, encoder=AppointmentEncoder, safe=False)
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=id).update(**content)
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({"error": "Appointment is invalid"}, status=400)
    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(['GET', 'POST'])
def create_technician(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Not valid"}
            )
            response.status_code = 400
            return response
