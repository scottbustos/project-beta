
from .models import Appointment, Technician, AutomobileVO
from common.json import ModelEncoder


class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin', 'import_href']


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ['name', 'employee_number', 'id']


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = ['customer', 'date', 'time',
                  'reason', 'vip', 'vin', 'id', 'technician']
    encoders = {"technician": TechnicianEncoder(), "automobile": AutomobileEncoder()
                }
