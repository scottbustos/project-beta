from django.urls import path
from .views import list_sales_person, show_sales_person, list_potential_customer, show_potential_customer, list_sales_records, show_sales_record, get_salespersons_record, car_inventory

urlpatterns = [
    path('employees/', list_sales_person, name='list_sales_person'),
    path('employee/<int:pk>/', show_sales_person, name='show_sales_person'),

    path('customers/', list_potential_customer, name='list_potential_customer'),
    path('customer/<int:pk>/', show_potential_customer, name='show_potential_customer'),

    path('sales/', list_sales_records, name='list_sales_record'),
    path('sales/<int:pk>/', show_sales_record, name='show_sales_record'),
    path('sales/employee/<int:pk>/', get_salespersons_record, name='get_sales_record'),
    path('sales/cars/', car_inventory, name='car_inventory'),

]
