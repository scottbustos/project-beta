import React, { useEffect, useState } from "react";

function TechnicianList() {
  const [technician, setTechnician] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnician(data.technician);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {technician.map((technician) => {
          return (
            <tr key={technician.id}>
              <td>{technician.name}</td>
              <td>{technician.employee_number}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
export default TechnicianList;
