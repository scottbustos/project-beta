import React, { useEffect, useState } from "react";

function AppointmentHistory() {
  const [filterValue, setFilterValue] = useState("");
  const [appointments, setAppointments] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    const data = await response.json();
    setAppointments(data.appointments);
  };

  useEffect(() => {
    getData();
  }, []);

  const handleChange = (e) => {
    setFilterValue(e.target.value);
  };

  const filterAppointments = () => {
    return appointments.filter((appointments) =>
      appointments.vin.toUpperCase().includes(filterValue)
    );
  };

  return (
    <div>
      <h1>Appointments History</h1>
      <input onChange={handleChange} placeholder="Search for a VIN" />
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Customer Name</th>
            <th>VIN</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIP</th>
          </tr>
        </thead>
        <tbody>
          {filterAppointments().map((appointments) => {
            return (
              <tr key={appointments.id}>
                <td>{appointments.customer}</td>
                <td>{appointments.vin}</td>
                <td>{appointments.date}</td>
                <td>{appointments.time}</td>
                <td>{appointments.technician.name}</td>
                <td>{appointments.reason}</td>
                <td>{appointments.vip ? "yes" : "no"}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentHistory;
