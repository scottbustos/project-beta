import React, { useEffect, useState } from "react";

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    const data = await response.json();
    setAppointments(data.appointments);
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (appointmentId) => {
    try {
      await fetch(`http://localhost:8080/api/appointments/${appointmentId}`, {
        method: "DELETE",
      });
      setAppointments(
        appointments.filter((appointment) => appointment.id !== appointmentId)
      );
    } catch (error) {
      console.error("Error deleting appointment:", error);
    }
    setAppointments(
      appointments.filter((appointment) => appointment.id !== appointmentId)
    );
  };

  return (
    <div>
      <h1>List of Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Customer Name</th>
            <th>VIN</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIP</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointments) => {
            return (
              <tr key={appointments.id}>
                <td>{appointments.customer}</td>
                <td>{appointments.vin}</td>
                <td>{appointments.date}</td>
                <td>{appointments.time}</td>
                <td>{appointments.technician.name}</td>
                <td>{appointments.reason}</td>
                <td>{appointments.vip ? "yes" : "no"}</td>
                <td>
                  <button
                    onClick={() => handleDelete(appointments.id)}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                  <button
                    onClick={() => handleDelete(appointments.id)}
                    className="btn btn-success"
                  >
                    Finish
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
