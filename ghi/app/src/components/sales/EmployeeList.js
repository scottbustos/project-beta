import React, { useEffect, useState } from 'react';

function EmployeeList () {
    const [employees, setEmployees] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/employees/")
        if (response.ok) {
            const data = await response.json()
            setEmployees(data.sales_person)
        }
    }

    useEffect(() =>{
        getData()
    }, [])


    const handleDelete = async (employeeId) => {
        try {
            await fetch(`http://localhost:8090/api/employees/${employeeId}`, {
                method: 'DELETE'
            });
            setEmployees(employees.filter(employee => employee.id !== employeeId))
        } catch (error) {
            console.error('Error deleting appointment:', error);
        }
        setEmployees(employees.filter(employee => employee.id !== employeeId))
    }

    return (
      <>
        <h1> Sales Employees </h1>
        <table className="table table-striped">
            <thead>
            <tr>
              <th>Salesperson</th>
              <th>Employee number</th>
            </tr>
          </thead>
          <tbody>
            {employees.map(employee => {
              return (
                <tr key={employee.href}>
                    <td>{employee.name}</td>
                    <td>{employee.employee_number }</td>
                    <td>
                        <button onClick={() => handleDelete(employees.id)} className="btn btn-danger">Delete</button>
                    </td>
                </tr>
              )
            })}
          </tbody>
        </table>
        </>
    )
}

export default EmployeeList
