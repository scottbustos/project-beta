import React, { useEffect, useState } from 'react';

function VehicleModelList () {
    const [cars, setCars] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/models/")
        if (response.ok) {
            const data = await response.json()
            setCars(data.models)
        }
    }

    useEffect(() =>{
        getData()
    }, [])

    return (
      <>
        <h1> Vehicle Models </h1>
        <table className="table table-striped">
            <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model</th>
            </tr>
          </thead>
          <tbody>
            {cars.map(car => {
              return (
                <tr key={car.href}>
                    <td>{car.manufacturer.name}</td>
                    <td>{car.name }</td>
                    <td>
                        <img src={car.picture_url} />
                    </td>
                </tr>
              )
            })}
          </tbody>
        </table>
        </>
    )
}

export default VehicleModelList
