import React, { useEffect, useState } from 'react';

function AutomobilesList () {
    const [cars, setCars] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/cars/")
        if (response.ok) {
            const data = await response.json()
            setCars(data.cars)
        }
    }

    useEffect(() =>{
        getData()
    }, [])

    return (
      <>
        <h1> Current Inventory </h1>
        <table className="table table-striped">
            <thead>
            <tr>
              <th>Vin</th>
              <th>Model</th>
              <th>Color</th>
            </tr>
          </thead>
          <tbody>
            {cars.map(car => {
              return (
                <tr key={car.href}>
                    <td>{car.vin}</td>
                    <td>{car.model}</td>
                    <td>{car.color}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
        </>
    )
}

export default AutomobilesList
