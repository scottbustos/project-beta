import { BrowserRouter, Routes, Route, Router } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './components/service/TechnicianForm';
import TechnicianList from './components/service/TechnicianList';
import AppointmentForm from './components/service/AppointmentForm';
import AppointmentList from './components/service/AppointmentList';
import AppointmentHistory from './components/service/AppointmentHistory';
import CustomerForm from './components/sales/CustomerForm';
import SalesPersonForm from './components/sales/SalePersonForm';
import SaleRecordForm from './components/sales/SaleRecordForm';
import SalesPersonRecordList from './components/sales/SalesPersonRecordList';
import SalesRecordList from './components/sales/SalesRecordList';
import CreateCarForm from './components/inventory/CreateCarForm';
import ManufacturerForm from './components/inventory/ManufacturerForm';
import VehicleModelForm from './components/inventory/VehicleModelForm';
import VehicleModelList from './components/inventory/VehicleModelList';
import ManufacturersList from './components/inventory/ManufacturersList';
import AutomobilesList from './components/inventory/CarInventoryList';
import EmployeeList from './components/sales/EmployeeList';
import CustomerList from './components/sales/CustomerList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="inventory">
            <Route path="models" element={<VehicleModelList />} />
            <Route path="manufacturers" element={<ManufacturersList />} />
            <Route path="automobiles" element={<AutomobilesList />} />
          </Route>
          <Route path ="create">
            <Route path="automobile" element={<CreateCarForm />} />
            <Route path="manufacturer" element={<ManufacturerForm />} />
            <Route path="model" element={<VehicleModelForm />} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm />} />
            <Route path="list" element={<CustomerList />} />
          </Route>
          <Route path="employees">
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesRecordList />} />
            <Route path="new" element={<SaleRecordForm />} />
            <Route path="record" element={<SalesPersonRecordList />} />
            <Route path="employees" element={<EmployeeList />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
            <Route index element={<TechnicianList />} />
          </Route>
          <Route path ="appointments">
            <Route path="new" element={<AppointmentForm />} />
            <Route index element={<AppointmentList />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
